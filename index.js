// var nombre = 'Ivan';
// var nombre = 'Carla';
// var nombre = '32132';

// let nombre = 'Ivan';
// nombre = 159;
// nombre = true;

// const nombre = 'Ivan';
// nombre = 'Carla';



// JSON 
const persona = {
  nombres: ['Ivan', 'Nicolas'],
  numeroDocumento: {
    numero: '12345678',
    complemento: '1C',
    extension: {
      departamento: 'La Paz',
      codigo: 'LP',
      municipio: {
        nombre: 'Alto Beni',
        codigoIne: 2248
      }
    }
  }
};

// console.log(listaFrutas[1]);
// console.log(persona['nombres']);
// console.log(persona.numeroDocumento.extension.municipio.nombre);
// Destructuring
const { numeroDocumento: nuevoNumero  } = persona;

const listaFrutas = ['Frutilla', 'Sandia', 'Pera', 'Manzana'];

const [fruta0, fruta1, fruta2, fruta3] = listaFrutas;

for(let i = 0; i < listaFrutas.length; i++) {
  if (listaFrutas[i] == 'Pera') {
    // console.log(listaFrutas[i])
  }
}

const posicion = listaFrutas.filter(fruta => fruta == 'Pera');

// ARROW FUNCTIONS
// function getNombre () {}

// const getNombre = () => {}


console.log(posicion)

// console.log(fruta2)