const auto = {
  marca: 'Tesla',
  color: 'Azul',
  tamanio: {
    alto: 1.90,
    ancho: 2.0 
  }
};

const nuevoAuto = JSON.parse(JSON.stringify(auto));

auto.tamanio.alto = 3.0;

// console.log(nuevoAuto);
// SPREAD OPERATOR (...)

const precio = { monto: 150000, unidad: 'Dolares' }

const autoConPrecio = { ...auto, ...precio, ...{ nroLlantas: 5 }, tieneVentanas: true }

const listaUno = [1, 2, 3, 4, 5];
const listaDos = [6, 7, 8, 9, 0];
const listaUnida = [...listaUno, ...listaDos, auto, true, 1.05, 'Algo mas'];
console.log(listaUnida)