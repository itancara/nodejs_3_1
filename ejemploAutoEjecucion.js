function ejemploPromesa () {  
  return new Promise((resolve, reject) => {
    // reject('ERRORRRRR!!!')
    setTimeout(() => {
      resolve('Ivan')
    }, 3000);
  })
}

function promesaDos () {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('OTRO VALOR')
    }, 5000)
  })
}

async function letraA () {
  return 'AAAA'
}

// (async function numeros () {
//   try {
//     console.log('PASO 1')
//     const respuesta = await ejemploPromesa(); // PASO 2
//     console.log(respuesta)

//     const respuestaFinal = await Promise.all([promesaDos(), promesaDos(), promesaDos(), promesaDos(), promesaDos()])

//     console.log(respuestaFinal)
//     console.log('PASO 3')
//   } catch (error) {
//     console.log(error)
//   }
// })(); 

function mostrarElementos (parametro1 = 1, parametro2 = 2, parametro3 = 3) {
  console.log(parametro1, parametro2, parametro3);
}

function mostrarElementosDestructuring ({ parametro1 = 1, parametro5 }) {
  console.log(parametro1, parametro5)
}

// mostrarElementos(1, undefined, 5);

const parametrosFuncion = { parametro3: 3, parametro2: 2, parametro1_1: 1, parametro5_5: 5, parametro4: 4 };
// mostrarElementosDestructuring(parametrosFuncion)

function getUsuario () {
  return null;
}

try {
  // const objetoUno = null || {}
  const objetoUno = getUsuario()
  
  // if (!objetoUno) { // null undefined '' 0
  //   throw new Error('No existe el regitro.')
  // }
  
  const { id } = objetoUno ? objetoUno : { id: 'NO TIENE' }
  // console.log(id)
} catch (error) {
  console.log(error.message);  
}


try {
  const auto = { propietario: null, color: 'ROJO' }
  console.log(auto.propietario?.documento.complemento)
} catch (error) {
  console.log(error.message)
}

