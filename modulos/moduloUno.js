function suma (num1, num2) {
  console.log('EJECUTANDO FUNCION SUMA.')
  return num1 + num2;
}

function resta (num1, num2) {
  return num1 - num2;
}

const VALOR_DEFECTO = 'SOY UN VALOR POR DEFECTO';

module.exports = {
  suma,
  resta,
  VALOR_DEFECTO
};