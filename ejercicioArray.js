const listaPersonas = [
  {
    nombre: 'Ivan',
    numeroDocumento: '123456',
    edad: 27
  },
  {
    nombre: 'Carla',
    numeroDocumento: '654321',
    edad: 20
  },
  {
    nombre: 'Andres',
    numeroDocumento: '531246',
    edad: 30
  },
  {
    nombre: 'Mariela',
    numeroDocumento: '642351',
    edad: 19
  }
];

const existePersona = listaPersonas.find((persona) => {
  return persona.numeroDocumento == '6543210'
});

const mayores = listaPersonas.filter(persona => persona.edad > 25);

const menores = listaPersonas.filter(persona => persona.edad < 25);

console.log(menores);