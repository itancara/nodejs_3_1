const persona = {
  nombre: 'Ivan',
  apellido: 'Tancara',
  imprimirNombre: function () {
    console.log(this.nombre)
  },
  imprimirApellido: () => {
    console.log(this.apellido)
  }
}


// EJEMPLOS DE CALLBACK

// function resultado (numero1, numero2, funcionSuma) {
//   funcionSuma(numero1, numero2);
// }

// const funSum = (num1, num2) => {
//   console.log(num1 + num2)
// }


// resultado(1, 3, funSum)

const listaNumeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

function buscarNumeroPar (lista, funcionEsPar) {
  for (let index = 0; index < lista.length; index++) {
    funcionEsPar(lista[index])
  }
}

const funPar = (numero) => {
  if (numero % 2 === 0) {
    console.log('El numero: '+ numero + ' es par.')
  }
}

// buscarNumeroPar(listaNumeros, funPar)

// EJEMPLOS DE PROMESAS

function ejemploPromesa () {  
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('Ivan')
    }, 3000);
  })
}

function promesaDos () {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('OTRO VALOR')
    }, 5000)
  })
}


console.log('Paso 1')
ejemploPromesa()
  .then((respuesta) => {
    console.log(respuesta)
    promesaDos().then((respuestaDos) => {
      console.log(respuestaDos)
      console.log('Paso 3')
      promesaDos().then((respuestaDos) => {
        console.log(respuestaDos)
        console.log('Paso 3')
        promesaDos().then((respuestaDos) => {
          console.log(respuestaDos)
          console.log('Paso 3')
          promesaDos().then((respuestaDos) => {
            console.log(respuestaDos)
            console.log('Paso 3')
            promesaDos().then((respuestaDos) => {
              console.log(respuestaDos)
              console.log('Paso 3')
            })
          })
        })
      })
    })
  })
  .catch((error) => {

  })



