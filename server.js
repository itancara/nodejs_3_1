const express = require('express');
const app = express();
const port = 3000;

// GET -> Obtener datos
// POST -> Crear datos
// PUT -> Actualizar datos
// DELETE -> Borrar datos
// PATCH -> Actualizar 1 solo dato

// 200, 201 Peticiones exitosas
// 400, 401, 403, 412
// 500, 502 ...

// Query   http://localhost:3000/mostrar-nombre?nombre=Carla&apellido=Condori
// Params  http://localhost:3000/mostrar-nombre/Carla
// Body    (POST, PUT, PATCH)
//         http://localhost:3000/mostrar-nombre      { nombre: Carla }

app.get('/mostrar-nombre', (req, res, next) => {
  // const nombre = req.query.nombre;
  // const apellido = req.query.apellido;
  const { nombre, apellido } = req.query
  console.log('PRIMERA RUTA')
  res.status(200).json({ 
    datos: `Hola desde microservicio ${nombre} ${apellido}.`
  })
});

app.get('/archivo/:nombreArchivo', (req, res, next) => {
  const { nombreArchivo } = req.params
  res.status(200).json({ nombreArchivo })
});



// EJEMPLO DE BUSQUEDA
const listaPersonas = [
  { nombre: 'Liliana Camargo Huanca', edad: 30 },
  { nombre: 'Katherine Ticona Castillo', edad: 31 },
  { nombre: 'Ivan Condori Castañeta', edad: 25 },
  { nombre: 'Alvaro Tenorio Garcia', edad: 18 },
  { nombre: 'Augusto Flores Mendoza', edad: 36 },
  { nombre: 'Angela Mamani Parra', edad: 27 },
  { nombre: 'Alisson Parisaca Huanca', edad: 22 },
  { nombre: 'Roger Flores Bustamante', edad: 24 },
  { nombre: 'Ximena Condori Calle', edad: 39 },
  { nombre: 'Mariela Condarco Garcia', edad: 19 }
];
// Por query edad, Buscar a la persona que coincida con esa edad
app.get('/personas', (req, res, next) => {
  try {
    const { edad } = req.query;
    if (!edad) {
      throw new Error('Debe enviar la edad a buscarse.')
    }
    const existePersona = listaPersonas.find(persona => persona.edad == edad);
    if (!existePersona) {
      throw new Error('La persona no existe.')
    }
    res.status(200).json({ existe: !!existePersona });
  } catch (error) {
    res.status(400).json({ respuesta: error.message })
  }
})

// Buscar a las personas con edad en un rango dado.
// http://localhost:3000/personas?edadInicio=27&edadFin=40
// undefined null 0 ''
// if (!edadFin) {
// }
app.get('/buscar-personas', (req, res, next) => {
  try {
    const edadInicio = req.query.edadInicio;
    const edadFin = req.query.edadFin;
  
    if (!edadInicio && !edadFin) {
      throw new Error('Debe enviar los parametros de busqueda.')
    }

    const listaPersonasEncontradas = listaPersonas.filter(persona => persona.edad > edadInicio && persona.edad < edadFin)

    res.status(200).json({ respuesta: listaPersonasEncontradas })

  } catch (error) {
      res.status(400).json({ respuesta: error.message })
  }
});

app.get('/ordenar-personas', (req, res, next) => {
  try {
    const listaNormal = [10, 1, 8, 9, 5, 6, 5, 4, 2, 3];
    console.log(listaNormal.sort((a, b) => a - b).reverse())

    const listaPersonasOrdenadas = listaPersonas.sort((a, b) => {
      if (a.edad < b.edad) {
        return -1;
      }
      if (a.edad > b.edad) {
        return 1;
      }
      return 0
    });

    res.status(200).json({ respuesta: listaPersonasOrdenadas })
  } catch (error) {
      res.status(400).json({ respuesta: error.message })
  }
});


app.listen(port, () => {
  console.log(`Funcionando en el puerto ${port}`)
});